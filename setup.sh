#!/bin/sh
echo
echo
echo "---------------------------------------------------------"
echo "We are now going to setup for deploying your application"
echo "Be sure to keep the default location when asked below"
echo "You may use any password you would like to setup your id"
echo "---------------------------------------------------------"
ssh-keygen
cat ~/.ssh/id_rsa.pub > ~/Desktop/id_rsa.txt

sed -i "s/;extension=php_curl.dll.*/extension=php_curl.dll/" /c/wamp/bin/php/php5.4.16/php.ini
sed -i "s/;extension=php_openssl.dll.*/extension=php_openssl.dll/" /c/wamp/bin/php/php5.4.16/php.ini

sed -i "s/;extension=php_curl.dll.*/extension=php_curl.dll/" /c/wamp/bin/apache/Apache2.4.4/bin/php.ini
sed -i "s/;extension=php_openssl.dll.*/extension=php_openssl.dll/" /c/wamp/bin/apache/Apache2.4.4/bin/php.ini

echo
echo
echo "---------------------------------------------------------"
echo "Make sure to email the id_rsa.txt file that is located on your Desktop to mike.blakeney@gmail.com"
echo
echo "Press ENTER and Restart Your Computer"
echo "---------------------------------------------------------"
read