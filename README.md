# Setup for Windows PHP Deployment

##First install git for Windows
The first thing we need to do is install git for Windows.  Use the link below to download the installer.

[Git for Windows](http://git-scm.com/download/win)

Once this has downloaded run the installer and keep all the default settings.

##Setup Identity
Now you need to run the [setup.sh](windows-php-deployment/raw/master/setup.sh) file that was provided to you.  You can download this
file by right clicking setup.sh link above and selecting "Save link as..." .  Save the file to your Desktop and run it.
Just double click setup.sh to run the setup.

1.  **When asked be sure to keep the default locations.**
2.  **You may use any password you would like to setup your id**
3.  **Make sure to email the id_rsa.txt file that is located on your Desktop to your support person**

##Install/Update your application files
To install or update your application files run the update.sh script that was provided to you. Make sure you use the password
that is associated with your identity that was setup above.

***

#Installing WAMP

###These are Instructions for setting up WAMP on a clean install of the WinXP VM provided by Microsoft
Before you can install WAMP, you must install two libraries.  Download and install these two libraries

1.  [Microsoft Visual C++ 2010 Redistributable Package x86](http://www.microsoft.com/en-us/download/details.aspx?id=5555)
2.  [Microsoft Visual C++ 2008 Redistributable Package x86](http://www.microsoft.com/en-us/download/details.aspx?id=5582)

Now you can download and install WAMP.

[Download WAMP here](http://sourceforge.net/projects/wampserver/files/WampServer%202/Wampserver%202.4/Wampserver2.4-x86.exe/download).

###Installing Composer
[Download composer here](https://getcomposer.org/Composer-Setup.exe)

You will find the php.exe file at:  C:\wamp\bin\php\php5.4.16\php.exe

###Download and Install Firefox and Chrome
1.  [Download Firefox here](http://www.getfirefox.com)
2.  [Download Chrome here](http://www.google.com/chrome)

